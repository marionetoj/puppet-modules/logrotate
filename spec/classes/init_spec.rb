# frozen_string_literal: true

require 'spec_helper'

describe 'logrotate' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('logrotate') }

      context 'with default parameters' do
        it do
          is_expected.to contain_class('logrotate::install')

          is_expected.to contain_package('logrotate').only_with_ensure('installed')
        end

        it do
          is_expected.to contain_class('logrotate::config')

          is_expected.to contain_logrotate__rule('global').only_with(
            'filename'  => '/etc/logrotate.conf',
            'template'  => 'logrotate/logrotate.conf.erb',
            'include'   => '/etc/logrotate.d',
            'frequency' => 'weekly',
            'rotate'    => 4,
            'create'    => true,
            'compress'  => true,
          )

          is_expected.to contain_file('/etc/logrotate.conf')

          is_expected.to contain_file('/etc/logrotate.d').only_with(
            'ensure'  => 'directory',
            'owner'   => 'root',
            'group'   => 'root',
            'mode'    => '0755',
            'purge'   => true,
            'recurse' => true,
          )

          ['btmp', 'wtmp'].each do |name|
            is_expected.to contain_logrotate__rule(name).only_with(
              'paths'        => ["/var/log/#{name}"],
              'frequency'    => 'monthly',
              'create'       => true,
              'create_mode'  => '0660',
              'create_owner' => 'root',
              'create_group' => 'utmp',
              'missingok'    => true,
              'rotate'       => 1,
            )

            is_expected.to contain_file("/etc/logrotate.d/#{name}").with_ensure('file')
          end
        end

        it do
          is_expected.to contain_class('logrotate::cron')

          is_expected.to contain_file('/etc/cron.daily/logrotate').only_with(
            'ensure' => 'file',
            'owner'  => 'root',
            'group'  => 'root',
            'mode'   => '0755',
            'source' => 'puppet:///modules/logrotate/logrotate.cron',
          )

          ['hourly', 'weekly', 'monthly'].each do |frequency|
            is_expected.to contain_file("/etc/cron.#{frequency}/logrotate").with_ensure('absent')
          end
        end
      end

      context 'with manage_package set to false' do
        let(:params) do
          {
            'manage_package' => false,
          }
        end

        it do
          is_expected.not_to contain_package('logrotate')
        end
      end

      context 'with non default package_name' do
        let(:params) do
          {
            'package_name' => 'test-package-name',
          }
        end

        it do
          is_expected.to contain_package('test-package-name').with_ensure('installed')
        end
      end

      context 'with package_ensure set to absent' do
        let(:params) do
          {
            'package_ensure' => 'absent',
          }
        end

        it do
          is_expected.to contain_package('logrotate').with_ensure('absent')
        end
      end

      context 'with non default config_file' do
        let(:params) do
          {
            'config_file' => '/etc/test-config-file',
          }
        end

        it do
          is_expected.to contain_logrotate__rule('global').with(
            'filename' => '/etc/test-config-file',
          )

          is_expected.to contain_file('/etc/test-config-file')
        end
      end

      context 'with non default config_dir' do
        let(:params) do
          {
            'config_dir' => '/etc/test-config-dir',
          }
        end

        it do
          is_expected.to contain_logrotate__rule('global').with(
            'include' => '/etc/test-config-dir',
          )

          is_expected.to contain_file('/etc/test-config-dir').with_ensure('directory')

          ['btmp', 'wtmp'].each do |name|
            is_expected.to contain_file("/etc/test-config-dir/#{name}")
          end
        end
      end

      context 'with manage_cron set to false' do
        let(:params) do
          {
            'manage_cron' => false,
          }
        end

        it do
          ['hourly', 'daily', 'weekly', 'monthly'].each do |frequency|
            is_expected.not_to contain_file("/etc/cron.#{frequency}/logrotate")
          end
        end
      end

      ['hourly', 'weekly', 'monthly'].each do |frequency|
        context "with cron_frequency set to #{frequency}" do
          let(:params) do
            {
              'cron_frequency' => frequency,
            }
          end

          it do
            is_expected.to contain_file("/etc/cron.#{frequency}/logrotate").with_ensure('file')
          end
        end
      end

      context 'with non default global_settings' do
        let(:params) do
          {
            'global_settings' => {
              'frequency' => 'monthly',
              'rotate'        => 1,
              'dateext'       => true,
            },
          }
        end

        it do
          is_expected.to contain_logrotate__rule('global').only_with(
            'filename'  => '/etc/logrotate.conf',
            'template'  => 'logrotate/logrotate.conf.erb',
            'include'   => '/etc/logrotate.d',
            'frequency' => 'monthly',
            'rotate'    => 1,
            'dateext'   => true,
          )
        end
      end

      context 'with some rules defined' do
        let(:params) do
          {
            'rules' => {
              'test1' => {
                'paths' => ['/var/log/test1'],
              },
              'test2' => {
                'paths'  => ['/var/log/test2'],
                'rotate' => 3,
              },
            },
          }
        end

        it do
          is_expected.to contain_logrotate__rule('test1').only_with(
            'paths' => ['/var/log/test1'],
          )

          is_expected.to contain_file('/etc/logrotate.d/test1').with_ensure('file')

          is_expected.to contain_logrotate__rule('test2').only_with(
            'paths'  => ['/var/log/test2'],
            'rotate' => 3,
          )

          is_expected.to contain_file('/etc/logrotate.d/test2').with_ensure('file')
        end
      end
    end
  end
end

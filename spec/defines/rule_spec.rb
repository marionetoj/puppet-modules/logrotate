# frozen_string_literal: true

require 'spec_helper'

describe 'logrotate::rule' do
  let(:pre_condition) do
    'include logrotate'
  end

  let(:title) { 'test' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with default parameters' do
        let(:params) do
          {}
        end

        it do
          is_expected.not_to compile
        end
      end

      context 'with paths set to /var/log/test.log' do
        let(:params) do
          {
            'paths' => ['/var/log/test.log'],
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test').with(
            'ensure' => 'file',
            'owner'  => 'root',
            'group'  => 'root',
            'mode'   => '0644',
          ).with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {

}
            EOS
          )
        end
      end

      context 'with paths set to /var/log/foo.log and /var/log/bar.log' do
        let(:params) do
          {
            'paths' => [
              '/var/log/foo.log',
              '/var/log/bar.log',
            ],
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test').with(
            'ensure' => 'file',
            'owner'  => 'root',
            'group'  => 'root',
            'mode'   => '0644',
          ).with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/foo.log
/var/log/bar.log {

}
            EOS
          )
        end
      end

      context 'with filename set to /etc/logrotate.d/test-filename' do
        let(:params) do
          {
            'filename' => '/etc/logrotate.d/test-filename',
            'paths'    => ['/var/log/test.log'],
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test-filename').with(
            'ensure' => 'file',
            'owner'  => 'root',
            'group'  => 'root',
            'mode'   => '0644',
          ).with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {

}
            EOS
          )
        end
      end

      context 'with create_mode set to 0644' do
        let(:params) do
          {
            'paths'       => ['/var/log/test.log'],
            'create'      => true,
            'create_mode' => '0644',
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test').with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  create 0644
}
            EOS
          )
        end

        context 'and create_owner set to root' do
          let(:params) do
            {
              'paths'        => ['/var/log/test.log'],
              'create'       => true,
              'create_mode'  => '0644',
              'create_owner' => 'root',
            }
          end

          it do
            is_expected.to contain_file('/etc/logrotate.d/test').with_content(
              <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  create 0644 root
}
              EOS
            )
          end

          context 'and create_group set to root' do
            let(:params) do
              {
                'paths'        => ['/var/log/test.log'],
                'create'       => true,
                'create_mode'  => '0644',
                'create_owner' => 'root',
                'create_group' => 'root',
              }
            end

            it do
              is_expected.to contain_file('/etc/logrotate.d/test').with_content(
                <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  create 0644 root root
}
                EOS
              )
            end
          end
        end
      end

      context 'with createolddir_mode set to 0644' do
        let(:params) do
          {
            'paths'             => ['/var/log/test.log'],
            'createolddir'      => true,
            'createolddir_mode' => '0644',
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test').with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  createolddir 0644
}
            EOS
          )
        end

        context 'and createolddir_owner set to root' do
          let(:params) do
            {
              'paths'              => ['/var/log/test.log'],
              'createolddir'       => true,
              'createolddir_mode'  => '0644',
              'createolddir_owner' => 'root',
            }
          end

          it do
            is_expected.to contain_file('/etc/logrotate.d/test').with_content(
              <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  createolddir 0644 root
}
              EOS
            )
          end

          context 'and createolddir_group set to root' do
            let(:params) do
              {
                'paths'              => ['/var/log/test.log'],
                'createolddir'       => true,
                'createolddir_mode'  => '0644',
                'createolddir_owner' => 'root',
                'createolddir_group' => 'root',
              }
            end

            it do
              is_expected.to contain_file('/etc/logrotate.d/test').with_content(
                <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  createolddir 0644 root root
}
                EOS
              )
            end
          end
        end
      end

      context 'with su_user set to root' do
        let(:params) do
          {
            'paths'   => ['/var/log/test.log'],
            'su'      => true,
            'su_user' => 'root',
          }
        end

        it do
          is_expected.to compile.with_all_deps

          is_expected.to contain_file('/etc/logrotate.d/test').with_content(
            <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  su root
}
            EOS
          )
        end

        context 'and su_group set to root' do
          let(:params) do
            {
              'paths'    => ['/var/log/test.log'],
              'su'       => true,
              'su_user'  => 'root',
              'su_group' => 'root',
            }
          end

          it do
            is_expected.to contain_file('/etc/logrotate.d/test').with_content(
              <<-EOS
# This file is managed by Puppet. DO NOT EDIT.

/var/log/test.log {
  su root root
}
              EOS
            )
          end
        end
      end
    end
  end
end

![CI status](https://gitlab.com/marionetoj/puppet-modules/logrotate/badges/main/pipeline.svg)
[![Contributor Covenant](https://img.shields.io/badge/CoC-Code%20Covenant%202.0-informational.svg)](CODE_OF_CONDUCT.md)

# logrotate

Manage Logrotate base configuration and rules.

## Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Contributing - Guide for contributing to the module](#development)

## Description

## Usage

This module's default values should be the same as your distribution defaults:

```puppet
include logrotate
```

But you can customize everything:

```puppet
class { '::logrotate':
  purge_unmanaged_rules => false,
  global_settings       => {
    compress     => true,
    create       => true,
    ifempty      => false,
    missingok    => true,
    rotate       => 30,
    frequency    => 'daily',
    copytruncate => true,
  },
}
```

You can then create rules with the `logrotate::rule` defined type:

```puppet
logrotate::rule { '/var/log/syslog':
  rotate        => 7,
  frequency     => 'daily',
  missingok     => true,
  ifempty       => false,
  delaycompress => true,
  compress      => true,
  postrotate    => [
    '/usr/lib/rsyslog/rsyslog-rotate',
  ]
}
```

## Reference

A complete reference can be found in the [REFERENCE.md file](REFERENCE.md).

## Limitations

This module has only been tested on Debian 9 and 10, but it should work with
older and newer version as well as with Ubuntu 18.04 and later.

## Contributing

This Puppet module is [Free Software](LICENCE.md), every contributions are
welcome.

Please note that this project is released with a [Contributor Code of
Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to
abide by its terms.

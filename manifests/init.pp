# @summary Install and configure logrotate
#
# @example Basic usage
#   class { '::logrotate':
#     purge_unmanaged_rules => false,
#     global_settings       => {
#       compress     => true,
#       create       => true,
#       ifempty      => false,
#       missingok    => true,
#       rotate       => 30,
#       frequency    => 'daily',
#       copytruncate => true,
#     },
#   }
#
# @param manage_package
#   Wether the package resource should be managed.
#
# @param package_name
#   Name of the package that installs logrotate.
#
# @param package_ensure
#   Define the ensure parameter for the logrotate package.
#
# @param config_file
#   Absolute path to logrotate's global configuration file (e.g.
#   /etc/logrotate.conf).
#
# @param config_dir
#   Absolute path to logrotate's rule files directory (e.g. /etc/logrotate.d).
#
# @param purge_unmanaged_rules
#   Weither or not to purge the configuration directory from unmanaged rules.
#
# @param manage_cron
#   Wether the cron entry should be managed.
#
# @param cron_frequency
#   The frequency at which cron should run.
#
# @param global_settings
#   The global settings to put in the global configuration file
#
# @param rules
#   A Hash of `logrotate::rule` to be passed to create_resources
#
class logrotate (
  Boolean $manage_package,
  String $package_name,
  String $package_ensure,

  Stdlib::AbsolutePath $config_file,
  Stdlib::AbsolutePath $config_dir,

  Boolean $purge_unmanaged_rules,

  Boolean $manage_cron,
  Pattern[/(hourly|daily|weekly|monthly)/] $cron_frequency,

  Hash $global_settings,

  Hash $rules = {},
) {
  contain logrotate::install
  contain logrotate::config
  contain logrotate::cron

  Class['logrotate::install']
  -> Class['logrotate::config']
  -> Class['logrotate::cron']
}

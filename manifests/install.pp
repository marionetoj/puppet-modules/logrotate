# @api private
class logrotate::install {
  assert_private()

  if $::logrotate::manage_package {
    package { $::logrotate::package_name:
      ensure => $::logrotate::package_ensure,
    }
  }
}

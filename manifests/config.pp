# @api private
class logrotate::config {
  assert_private()

  logrotate::rule { 'global':
    filename => $::logrotate::config_file,
    template => 'logrotate/logrotate.conf.erb',
    include  => $::logrotate::config_dir,
    *        => $::logrotate::global_settings,
  }

  file { $::logrotate::config_dir:
    ensure  => directory,
    purge   => $::logrotate::purge_unmanaged_rules,
    recurse => $::logrotate::purge_unmanaged_rules,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }

  logrotate::rule {
    default:
      frequency    => 'monthly',
      create       => true,
      create_mode  => '0660',
      create_owner => 'root',
      create_group => 'utmp',
      missingok    => true,
      rotate       => 1,
    ;
    'wtmp':
      paths => ['/var/log/wtmp'],
    ;
    'btmp':
      paths => ['/var/log/btmp'],
    ;
  }

  create_resources('logrotate::rule', $::logrotate::rules)
}

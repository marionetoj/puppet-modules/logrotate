# @api private
class logrotate::cron {
  assert_private()

  if $::logrotate::manage_cron {
    $cron_files = ['hourly', 'daily', 'weekly', 'monthly'].map |$frequency| {
      "/etc/cron.${frequency}/logrotate"
    }

    file {
      $cron_files.filter |$name| { $::logrotate::cron_frequency in $name }:
        ensure => file,
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
        source => 'puppet:///modules/logrotate/logrotate.cron',
      ;
      $cron_files.filter |$name| { !($::logrotate::cron_frequency in $name) }:
        ensure => absent,
      ;
    }
  }
}
